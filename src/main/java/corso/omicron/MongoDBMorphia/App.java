package corso.omicron.MongoDBMorphia;

import java.util.HashSet;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import corso.omicron.MongoDBMorphia.models.Editore;
import corso.omicron.MongoDBMorphia.models.Libro;

public class App 
{
    public static void main( String[] args )
    {
    	
    	try {
    		MongoClientURI connectionUri = new MongoClientURI("mongodb://omicron:cicciopasticcio@cluster0-shard-00-00.9giyg.mongodb.net:27017,cluster0-shard-00-01.9giyg.mongodb.net:27017,cluster0-shard-00-02.9giyg.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=atlas-ev4u8w-shard-0&authSource=admin&retryWrites=true&w=majority");
        	MongoClient client = new MongoClient(connectionUri);
        	
        	Morphia morphia = new Morphia();
        	morphia.mapPackage("corso.omicron.MongoDBMorphia.models");
        	
        	Datastore ds = morphia.createDatastore(client, "Libreria");
        	ds.ensureIndexes();
        	
        	
        	//INserimento
//        	Editore edObj = new Editore(new ObjectId(), "Edizioni giovanni", "Roma");
//        	
//        	Libro temObj = new Libro("1234-5678-9874", "La Tempesta", 11.5d, edObj);
//        	Libro othObj = new Libro("7896-65478-655", "Othello", 10.2d, edObj);
//        	
//        	temObj.setCorrelati(new HashSet<Libro>());
//        	temObj.getCorrelati().add(othObj);
//        	
//        	ds.save(othObj);
//        	ds.save(edObj);
//        	ds.save(temObj);
//        	
//        	System.out.println("Operazione effettuata con successo!");
        	
        	//Ricerca
        	List<Libro> risultato = ds.find(Libro.class).field("titolo").contains("Tempesta").asList();
        	
        	for(int i=0; i<risultato.size(); i++) {
        		Libro temp = risultato.get(i);
        		System.out.println(temp);
        	}
        	
        	
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
    	
    
    }
}
