package corso.omicron.MongoDBMorphia.models;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Validation;

@Entity("Libri")
@Indexes({ @Index(fields = @Field("titolo")) })
@Validation("{ prezzo : { $gt: 0 }}")
public class Libro {

	@Id
	private String isbn;
	
	@Property
	private String titolo;
	
	@Property
	private double prezzo;
	
	@Property
	private LocalDate pubData;
	
	@Embedded						//<------
	private Editore editore;
	
	@Reference						//<------
	private Set<Libro> correlati = new HashSet<Libro>();
	
	public Libro() {
		
	}

	public Libro(String isbn, String titolo, double prezzo, Editore editore) {
		super();
		this.isbn = isbn;
		this.titolo = titolo;
		this.prezzo = prezzo;
		this.editore = editore;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	public LocalDate getPubData() {
		return pubData;
	}

	public void setPubData(LocalDate pubData) {
		this.pubData = pubData;
	}

	public Editore getEditore() {
		return editore;
	}

	public void setEditore(Editore editore) {
		this.editore = editore;
	}

	public Set<Libro> getCorrelati() {
		return correlati;
	}

	public void setCorrelati(Set<Libro> correlati) {
		this.correlati = correlati;
	}

	@Override
	public String toString() {
		return "Libro [isbn=" + isbn + ", titolo=" + titolo + ", prezzo=" + prezzo + ", pubData=" + pubData
				+ ", editore=" + editore + ", correlati=" + correlati + "]";
	}
	
}
